#ifndef __CUST_MAG_H__
#define __CUST_MAG_H__

#include <linux/types.h>

#define M_CUST_I2C_ADDR_NUM 2

struct mag_hw {
    int i2c_num;
    int direction;
    int power_id;
    int power_vol;
    unsigned char	i2c_addr[M_CUST_I2C_ADDR_NUM]; /*!< i2c address list,for chips which has different addresses with different HW layout */
    int power_vio_id;
    int power_vio_vol;
    bool is_batch_supported;
};

static struct mag_hw cust_mag_hw = {
    .i2c_num = 3,
    .direction = 2,
    .power_id = MT65XX_POWER_NONE,  /*!< LDO is not used */
    .i2c_addr = {'\0', '\0'},
    .power_vol= VOL_DEFAULT,        /*!< LDO is not used */
    .power_vio_id= 0, //old value 16                /*!< don't enable low pass fileter */
    .power_vio_vol = 0,
    .is_batch_supported = 0,
};

struct mag_hw* get_cust_mag_hw(void)
{
    return &cust_mag_hw;
};

struct mag_hw* get_mag_dts_func(const char *, struct mag_hw*);
#endif 
