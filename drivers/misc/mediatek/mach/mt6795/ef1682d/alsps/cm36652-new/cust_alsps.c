#include <linux/types.h>
#include <mach/mt_pm_ldo.h>
#include <cust_alsps.h>
#include <mach/upmu_common.h>

static struct alsps_hw cust_alsps_hw = {
    .i2c_num    = 3,
	.i2c_addr 	= {5,0,0,0},
    .polling_mode =0,
	.polling_mode_ps =1,
	.polling_mode_als =0x90,
    .power_id   = 1,    /*LDO is not used*/
    .power_vol  = 2800,          /*LDO is not used*/
    //.i2c_addr   = {0x0C, 0x48, 0x78, 0x00},
    /* MTK: modified to support AAL */
    .als_level  = {9, 0x24, 0x3B, 0x52, 0x84, 0xCD, 0x111, 0x1F4, 0x34D, 0x470, 0x609, 0x93C, 0x122F, 0x1B46, 0xA}, 
    .als_value  = {0xA, 0x28, 0x41, 0x5A, 0x91, 0xE1, 0x12C, 0x226, 0x3A2, 0x4E2, 0x6A4, 0xA28, 0x1400, 0x1E00, 0x2800, 0}, 
    .ps_threshold = 0x31,
    .als_window_loss = 0x3A,
    .ps_threshold_high = 0xFF,
    .ps_threshold_low = 7,
    .als_threshold_high = 0x6A4,
    .als_threshold_low =0x5DC,
.als_power_vio_id = 0,
.als_power_vio_vol = 0,
.ps_power_vdd_id =3,
.ps_power_vdd_vol= 7,
.ps_power_vio_id =0xFFFFFFFF, 
.ps_power_vio_vol = 0,
.power_lp_mode_ctrl = 0,
    .is_batch_supported_ps = false,
    .is_batch_supported_als = false,
};
struct alsps_hw *get_cust_alsps_hw(void) {
    return &cust_alsps_hw;
}

