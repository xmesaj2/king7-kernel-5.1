#ifndef BUILD_LK
#include <linux/string.h>
#include <linux/kernel.h>
#endif
#include "lcm_drv.h"

#ifdef BUILD_LK
	#include <platform/upmu_common.h>
	#include <platform/mt_gpio.h>
	#include <platform/mt_i2c.h> 
	#include <platform/mt_pmic.h>
	#include <string.h>
#elif defined(BUILD_UBOOT)
    #include <asm/arch/mt_gpio.h>
#else
	#include <mach/mt_pm_ldo.h>
    #include <mach/mt_gpio.h>
#endif
#include <cust_gpio_usage.h>
#include <cust_i2c.h>
#include "tps65132_i2c.h"
#ifdef BUILD_LK
#define LCD_DEBUG(fmt)  dprintf(CRITICAL,fmt)
#else
#define LCD_DEBUG(fmt)  printk(fmt)
#endif

//static unsigned char lcd_id_pins_value = 0xFF;
static const unsigned char LCD_MODULE_ID = 0x01; //  haobing modified 2013.07.11
// ---------------------------------------------------------------------------
//  Local Constants
// ---------------------------------------------------------------------------
#define LCM_DSI_CMD_MODE									0
#define FRAME_WIDTH  										(1440)
#define FRAME_HEIGHT 										(2560)
#define GPIO_65132_EN 0x80000082 //GPIO_LCD_BIAS_ENP_PIN
#define GPIO_RESET_EN_PIN		(GPIO131 | 0x80000000)
#define REGFLAG_PORT_SWAP									0xFFFA
#define REGFLAG_DELAY             							0xFFFC	
#define REGFLAG_END_OF_TABLE      							0xFFFD   // END OF REGISTERS MARKER

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif
//static unsigned int lcm_esd_test = FALSE;      ///only for ESD test
// ---------------------------------------------------------------------------
//  Local Variables
// ---------------------------------------------------------------------------

static const unsigned int BL_MIN_LEVEL =20;
static LCM_UTIL_FUNCS lcm_util;

#define SET_RESET_PIN(v)    								(lcm_util.set_reset_pin((v)))
#define MDELAY(n) 											(lcm_util.mdelay(n))

// ---------------------------------------------------------------------------
//  Local Functions
// ---------------------------------------------------------------------------
#define dsi_set_cmd_by_cmdq_dual(handle,cmd,count,ppara,force_update)    lcm_util.dsi_set_cmdq_V23(handle,cmd,count,ppara,force_update);
#define dsi_set_cmdq_V2(cmd, count, ppara, force_update)	lcm_util.dsi_set_cmdq_V2(cmd, count, ppara, force_update)
#define dsi_set_cmdq(pdata, queue_size, force_update)		lcm_util.dsi_set_cmdq(pdata, queue_size, force_update)
#define wrtie_cmd(cmd)										lcm_util.dsi_write_cmd(cmd)
#define write_regs(addr, pdata, byte_nums)					lcm_util.dsi_write_regs(addr, pdata, byte_nums)
#define read_reg(cmd)										lcm_util.dsi_dcs_read_lcm_reg(cmd)
#define read_reg_v2(cmd, buffer, buffer_size)   			lcm_util.dsi_dcs_read_lcm_reg_v2(cmd, buffer, buffer_size)    
#define dsi_swap_port(swap)   								lcm_util.dsi_swap_port(swap)

#ifndef BUILD_LK
#include <linux/kernel.h>
#include <linux/module.h>  
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/i2c.h>
#include <linux/irq.h>
//#include <linux/jiffies.h>
#include <linux/uaccess.h>
//#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/platform_device.h>
/***************************************************************************** 
* Define
*****************************************************************************/

#define TPS_I2C_BUSNUM  I2C_I2C_LCD_BIAS_CHANNEL//for I2C channel 0
#define I2C_ID_NAME "tps65132"
#define TPS_ADDR 0x3E

/***************************************************************************** 
* GLobal Variable
*****************************************************************************/
static struct i2c_board_info __initdata tps65132_board_info = {I2C_BOARD_INFO(I2C_ID_NAME, TPS_ADDR)};
static struct i2c_client *tps65132_i2c_client = NULL;


/***************************************************************************** 
* Function Prototype
*****************************************************************************/ 
static int tps65132_probe(struct i2c_client *client, const struct i2c_device_id *id);
static int tps65132_remove(struct i2c_client *client);
/***************************************************************************** 
* Data Structure
*****************************************************************************/

struct tps65132_dev	{	
struct i2c_client	*client;

};

static const struct i2c_device_id tps65132_id[] = {
{ I2C_ID_NAME, 0 },
{ }
};

//#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,36))
//static struct i2c_client_address_data addr_data = { .forces = forces,};
//#endif
static struct i2c_driver tps65132_iic_driver = {
    .id_table	= tps65132_id,
    .probe		= tps65132_probe,
    .remove		= tps65132_remove,
    //.detect		= mt6605_detect,
    .driver		= {
    .owner	= THIS_MODULE,
    .name	= "tps65132",
},
};
/***************************************************************************** 
* Extern Area
*****************************************************************************/ 

/***************************************************************************** 
* Function
*****************************************************************************/ 


static int tps65132_probe(struct i2c_client *client, const struct i2c_device_id *id)
{  
    printk( "tps65132_iic_probe\n");
    printk("TPS: info==>name=%s addr=0x%x\n",client->name,client->addr);
    tps65132_i2c_client  = client;		
    return 0;      
}


static int tps65132_remove(struct i2c_client *client)
{  	
    printk( "tps65132_remove\n");
    tps65132_i2c_client = NULL;
    i2c_unregister_device(client);
    return 0;
}

 
int tps65132_write_bytes(unsigned char addr, unsigned char value)
{	
    int ret = 0;
    struct i2c_client *client = tps65132_i2c_client;

	if(client == NULL)
	{
		printk("ERROR!!tps65132_i2c_client is null\n");
		return 0;
	}
	
    char write_data[2]={0};	
    write_data[0]= addr;
    write_data[1] = value;
    ret=i2c_master_send(client, write_data, 2);
    if(ret<0)
        printk("tps65132 write data fail !!\n");	
    return ret ;
}
EXPORT_SYMBOL_GPL(tps65132_write_bytes);


/*
* module load/unload record keeping
*/

static int __init tps65132_iic_init(void)
{
    printk( "tps65132iic_init\n");
    i2c_register_board_info(TPS_I2C_BUSNUM, &tps65132_board_info, 1);
    printk( "tps65132_iic_init2\n");
    i2c_add_driver(&tps65132_iic_driver);
    printk( "tps65132_iic_init success\n");	
    return 0;
}

static void __exit tps65132_iic_exit(void)
{
    printk( "tps65132_iic_exit\n");
    i2c_del_driver(&tps65132_iic_driver);  
}

module_init(tps65132_iic_init);
module_exit(tps65132_iic_exit);

MODULE_AUTHOR("Maciej Klimas");
MODULE_DESCRIPTION("MTK TPS65132 I2C Driver reversed");
MODULE_LICENSE("GPL"); 

#endif

struct LCM_setting_table {
    unsigned int cmd;
    unsigned char count;
    unsigned char para_list[64];
};

static struct LCM_setting_table lcm_initialization_setting[] = 
{
{0x36, 1, {0x00,0x01,0x00,0x00,0x00}},
{0xb0, 1, {0x00}},
{0xd6, 1, {0x01}},
{0xb3, 3, {0x14,0x00,0x00}},
{0xb4, 1, {0x00}},
{0xb6, 2, {0x3a,0xd3}},
{0xbe, 1, {0x04}},
{0xc3, 3, {0x00,0x00,0x00}},
{0xc5, 1, {0x00}},
{0xc0, 4, {0x00,0x00,0x00,0x00}},
{0xc1, 35, {0x00,0x61,0x00,0x20,0x8c,0xa4,0x16,0xfb,0xbf,0x98,0x83,0x9a,0x7b,0xcf,0x35,0x74,0x4c,0xf9,0x9f,0x2d,0x95,0x88,0x00,0x00,0x00,0x00,0x00,0x10,0x02,0x63,0x23,0x03,0x00,0xff,0x11}},
{0xc2, 8, {0x0a,0x0a,0x00,0x08,0x08,0xf0,0x00,0x04}},
{0xc4, 14, {0x70,0x00,0x00,0x00,0x33,0x00,0x00,0x33,0x00,0x00,0x00,0x01,0x05,0x01}},
{0xc6, 21, {0x5a,0x29,0x29,0x01,0x04,0x00,0x05,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0f,0x13,0x07,0x5a}},
{0xcb, 15, {0x7f,0xe0,0x07,0xfe,0x00,0x00,0x00,0x00,0x54,0xe0,0x07,0x2a,0xc8,0x00,0x00}},
{0xcc, 1, {0x11}},
{0xd7, 13, {0x82,0xff,0x21,0x8e,0x8c,0xf1,0x87,0x3f,0x7e,0x10,0x00,0x00,0x8f}},
{0xd9, 2, {0x00,0x00}},
{0xd0, 4, {0x11,0x12,0x12,0xfd}},
{0xd2, 16, {0xcd,0x2b,0x2b,0x33,0x12,0x33,0x33,0x33,0x77,0x77,0x33,0x33,0x33,0x00,0x00,0x00}},
{0xd5, 7, {0x06,0x00,0x00,0x01,0x26,0x01,0x26}},
{0xc7, 30, {0x00,0x0a,0x10,0x19,0x28,0x37,0x42,0x54,0x38,0x40,0x4c,0x59,0x63,0x6b,0x7f,0x00,0x0a,0x10,0x19,0x28,0x37,0x42,0x54,0x38,0x40,0x4c,0x59,0x63,0x6b,0x7f}},
{0xc8, 19, {0x01,0x00,0x00,0x00,0x00,0xe2,0xef,0x00,0x00,0x00,0x00,0xfc,0xef,0x00,0x00,0x00,0x00,0xb8,0xef}},
{0xca, 24, {0x81,0x70,0x80,0xdc,0xf0,0xdc,0x90,0x0c,0x08,0x00,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0a,0x4a,0x37,0x80,0x55,0xc8}}, // or without dis
{0xb8, 7, {0x57,0x3d,0x19,0x1e,0x0a,0x50,0x50}},
{0xb9, 7, {0x6f,0x3d,0x28,0x3c,0x14,0xc8,0xc8}},
{0xba, 7, {0xb5,0x33,0x41,0x64,0x23,0xa0,0xa0}},
{0xbb, 2, {0x14,0x14}},
{0xbc, 2, {0x37,0x32}},
{0xbd, 2, {0x64,0x32}},
{0xce, 25, {0x55,0x40,0x49,0x53,0x59,0x5e,0x63,0x68,0x6e,0x74,0x7e,0x8a,0x98,0xa8,0xbb,0xd0,0xff,0x04,0x00,0x04,0x04,0x00,0x00,0x00,0x00}},
{0xf4, 9, {0xfb,0xff,0x05,0x85,0x3d,0x01,0x19,0x19,0x00}},
{0x35, 1, {0x00}},
{0x29, 0, {0x00,0x00}},
{0xfc, 20, {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}},
{0x11, 0, {0x00,0x00}},
{0xfc, 120, {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xfd,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}},
{REGFLAG_END_OF_TABLE, 0x00, {}}
};

static struct LCM_setting_table lcm_suspend_setting[] = {
	{0x28,0,{}},
	{0x10,0,{}},
	{REGFLAG_DELAY, 120, {}}
};

static void push_table(struct LCM_setting_table *table, unsigned int count, unsigned char force_update)
{
    unsigned int i;

    for(i = 0; i < count; i++)
    {
        unsigned cmd;
        cmd = table[i].cmd;
        
        switch (cmd) {
            case REGFLAG_DELAY :
#ifdef BUILD_LK
    			dprintf(0, "[LK]REGFLAG_DELAY\n");
#endif
           		if(table[i].count <= 10)
           			MDELAY(table[i].count);
            	else
            		MDELAY(table[i].count);
           		break;

            case REGFLAG_END_OF_TABLE :
            	break;
			case REGFLAG_PORT_SWAP:	
#ifdef BUILD_LK
    			dprintf(0, "[LK]push_table end\n");
#endif
				dsi_swap_port(1);
                break;
            default:
            dsi_set_cmdq_V2(cmd, table[i].count, table[i].para_list, force_update);
        }
    }
}

static void tps65132_enable(bool enable)
{
    int i;
    mt_set_gpio_mode(GPIO_MHL_RST_B_PIN, GPIO_MODE_00);
    mt_set_gpio_dir(GPIO_MHL_RST_B_PIN, GPIO_DIR_OUT);
    mt_set_gpio_mode(GPIO_MHL_EINT_PIN, GPIO_MODE_00);
    mt_set_gpio_dir(GPIO_MHL_EINT_PIN, GPIO_DIR_OUT);
    if (enable){
        mt_set_gpio_out(GPIO_MHL_EINT_PIN, GPIO_OUT_ONE);
        MDELAY(12);
        mt_set_gpio_out(GPIO_MHL_RST_B_PIN, GPIO_OUT_ONE);
        MDELAY(12);
        for (i=0; i < 3; i++){
            if ((tps65132_write_bytes(0, 0xF) & 0x1f)==0) break;
            MDELAY(5);
        }
    }else{
        mt_set_gpio_out(GPIO_MHL_RST_B_PIN, GPIO_OUT_ZERO);
        MDELAY(12);
        mt_set_gpio_out(GPIO_MHL_EINT_PIN, GPIO_OUT_ZERO);
        MDELAY(12);
    }
}

// ---------------------------------------------------------------------------
//  LCM Driver Implementations
// ---------------------------------------------------------------------------

static void lcm_set_util_funcs(const LCM_UTIL_FUNCS *util)
{
    memcpy(&lcm_util, util, sizeof(LCM_UTIL_FUNCS));
}


static void lcm_get_params(LCM_PARAMS *params)
{
    memset(params, 0, sizeof(LCM_PARAMS));

    params->width = 0x5a0;
    params->dsi.horizontal_active_pixel = 0x5a0;
    params->dsi.vertical_frontporch = 8;
    params->dsi.horizontal_sync_active = 8;
    params->dsi.PLL_CLOCK = 500;
    params->type = LCM_TYPE_DSI;
    params->height = 0xa00;
    params->lcm_if = LCM_INTERFACE_DSI_DUAL;
    params->lcm_cmd_if = LCM_INTERFACE_DSI0;
    params->dsi.mode = BURST_VDO_MODE;
    params->dsi.dual_dsi_type = DUAL_DSI_VDO;
    params->dsi.LANE_NUM = LCM_FOUR_LANE;
    params->dsi.data_format.format = LCM_DSI_FORMAT_RGB888;
    params->dsi.packet_size = 0x100;
    params->dsi.ssc_disable = 1;
    params->dsi.PS = LCM_PACKED_PS_24BIT_RGB888;
    params->dsi.vertical_sync_active = 4;
    params->dsi.vertical_backporch = 4;
    params->dsi.vertical_active_line = 0xa00;
    params->dsi.horizontal_backporch = 0x3c;
    params->dsi.ufoe_enable = 1;
    params->dsi.horizontal_frontporch = 0x96;
    params->dsi.ufoe_params.lr_mode_en = 1;
    params->dsi.esd_check_enable = 1;
    params->dsi.lcm_esd_check_table[0].cmd = 0xa;
    params->dsi.lcm_esd_check_table[0].count = 1;
    params->dsi.lcm_esd_check_table[0].para_list[0] = 0x1c;

    params->dsi.lane_swap_en = 1;
    params->dsi.lane_swap[MIPITX_PHY_PORT_1][MIPITX_PHY_LANE_0] = MIPITX_PHY_LANE_2;
    params->dsi.lane_swap[MIPITX_PHY_PORT_1][MIPITX_PHY_LANE_1] = MIPITX_PHY_LANE_CK;
    params->dsi.lane_swap[MIPITX_PHY_PORT_1][MIPITX_PHY_LANE_3] = MIPITX_PHY_LANE_1;
    params->dsi.lane_swap[MIPITX_PHY_PORT_1][MIPITX_PHY_LANE_CK] = MIPITX_PHY_LANE_3;
    params->dsi.lane_swap[MIPITX_PHY_PORT_1][MIPITX_PHY_LANE_RX] = MIPITX_PHY_LANE_2;

    params->dsi.lane_swap[MIPITX_PHY_PORT_0][MIPITX_PHY_LANE_1] = MIPITX_PHY_LANE_3;
    params->dsi.lane_swap[MIPITX_PHY_PORT_0][MIPITX_PHY_LANE_2] = MIPITX_PHY_LANE_2;
    params->dsi.lane_swap[MIPITX_PHY_PORT_0][MIPITX_PHY_LANE_3] = MIPITX_PHY_LANE_1;
    params->dsi.lane_swap[MIPITX_PHY_PORT_0][MIPITX_PHY_LANE_CK] = MIPITX_PHY_LANE_CK;
}

static void lcm_init(void)
{
	
  SET_RESET_PIN(1);
  MDELAY(2);
  SET_RESET_PIN(0);
  MDELAY(10);
  mt_set_gpio_mode(0x80000083,0);
  mt_set_gpio_dir(0x80000083,1);
  mt_set_gpio_out(0x80000083,1);
  MDELAY(10);
  mt_set_gpio_mode(0x80000082,0);
  mt_set_gpio_dir(0x80000082,1);
  mt_set_gpio_out(0x80000082,1);
  MDELAY(10);
  SET_RESET_PIN(1);
  MDELAY(0X14);

  push_table(lcm_initialization_setting, sizeof(lcm_initialization_setting) / sizeof(struct LCM_setting_table),1);
}

static void lcm_suspend(void)
{
  push_table(lcm_suspend_setting, sizeof(lcm_suspend_setting) / sizeof(struct LCM_setting_table), 1);
  SET_RESET_PIN(0);
  mt_set_gpio_mode(0x80000082,0);
  mt_set_gpio_dir(0x80000082,1);
  mt_set_gpio_out(0x80000082,0);
  MDELAY(10);
  mt_set_gpio_mode(0x80000083,0);
  mt_set_gpio_dir(0x80000083,1);
  mt_set_gpio_out(0x80000083,0);
  MDELAY(10);
}

static void lcm_resume(void)
{
	lcm_init();
}

#define LCM_ID_R63419 (0x3419)

static unsigned int lcm_compare_id(void)
{
	unsigned char buffer[5];
	unsigned int array[16];  
	int i;

    mt_set_gpio_mode(0x80000090,0);
    mt_set_gpio_dir(0x80000090,0);

	unsigned int lcd_id = 0;

    mt_set_gpio_pull_enable(0x80000090,0);
    int in = mt_get_gpio_in(0x80000090);
    if (in == 1)
    {
        SET_RESET_PIN(1);
        MDELAY(10);
        SET_RESET_PIN(0);
        MDELAY(10);

        mt_set_gpio_mode(0x80000083,0);
        mt_set_gpio_dir(0x80000083,1);
        mt_set_gpio_out(0x80000083,1);
        MDELAY(10);
        mt_set_gpio_mode(0x80000082,0);
        mt_set_gpio_dir(0x80000082,1);
        mt_set_gpio_out(0x80000082,1);

        MDELAY(10);
        SET_RESET_PIN(1);
        MDELAY(10);

        array[0] = 0x00053700;// read id return two byte,version and id
        dsi_set_cmdq(array, 1, 1);
    
        read_reg_v2(0xBF, buffer, 5);
        MDELAY(20);
        lcd_id = (buffer[2] << 8 )| buffer[3];

        printk("%s, kernel r63419 horse debug: r63419 id = 0x%08x\n", __func__, lcd_id);
    }
    if(lcd_id == LCM_ID_R63419)
        return 1;
    else
        return 0;
}

LCM_DRIVER r63419_wqhd_truly_phantom_vdo_lcm_drv=
{
    .name           = "r63419_wqhd_truly_auo_vdo",
    .set_util_funcs = lcm_set_util_funcs,
    .get_params     = lcm_get_params,
    .init           = lcm_init,
    .suspend        = lcm_suspend,
    .resume         = lcm_resume,
    .compare_id     = lcm_compare_id,  
    //.init_power		= lcm_init_power,
   // .resume_power   = lcm_resume_power,
   // .suspend_power  = lcm_suspend_power,
    // .ata_check		= lcm_ata_check,
   // .set_backlight_cmdq  = lcm_setbacklight_cmdq,
//#if (LCM_DSI_CMD_MODE)
//    .update         = lcm_update,
//#endif

};
/* END PN:DTS2013053103858 , Added by d00238048, 2013.05.31*/
